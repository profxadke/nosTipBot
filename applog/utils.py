"""Original Credit: https://github.com/tiangolo/fastapi/issues/290#issuecomment-500119238"""

import logging.config
import os

import yaml


def read_logging_config(default_path="logging.yaml", env_key="LOG_CFG"):
    """Load the logging config into memory"""
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, "rt") as f:
            logging_config = yaml.safe_load(f.read())
        return logging_config
    return None


def setup_logging(logging_config, default_level=logging.INFO):
    """Configure logging"""
    if logging_config:
        logging.config.dictConfig(logging_config)
    else:
        logging.basicConfig(level=default_level)
