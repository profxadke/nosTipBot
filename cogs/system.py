"""System Module from Bot Template"""
import sys
import subprocess
import logging

from discord.ext import commands
import discord

import core.common as common

logger = logging.getLogger(__name__)


class System(commands.Cog):
    """System Cog from Bot Template"""
    def __init__(self, bot):
        self.bot = bot
        self.commit = subprocess.run(["git", "show", "-s", "--format=%h"],
                                     capture_output=True,
                                     encoding="utf-8", check=True).stdout.strip()

    @commands.group()
    async def bot_group(self, ctx):
        """Command group for core bot commands"""
        if ctx.invoked_subcommand is None:
            await ctx.send('No subcommand invoked.')

    @bot_group.command()
    async def ping(self, ctx):
        """Ping the bot"""
        await ctx.message.delete()
        embed = discord.Embed(title="Pong!",
                              description=str(round(self.bot.latency * 1000, 1)) + "ms",
                              colour=common.random_rgb())
        embed.set_footer(text=f"requested by {ctx.author}",
                         icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)

    @bot_group.command()
    @commands.is_owner()
    async def stop(self, ctx):
        """Stop the bot"""
        await ctx.message.delete()
        embed = discord.Embed(title="Stopping Bot!",
                              color=common.random_rgb())
        embed.set_footer(text=f"requested by {ctx.author}",
                         icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)
        sys.exit()

    @bot_group.command()
    @commands.is_owner()
    async def restart(self, ctx):
        """Restart the bot"""
        await ctx.message.delete()
        embed = discord.Embed(title="Restarting Bot!",
                              color=common.random_rgb())
        embed.set_footer(text=f"requested by {ctx.author}",
                         icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)
        sys.exit(26)

    @bot_group.command()
    async def version(self, ctx):
        """Get bot version"""
        await ctx.message.delete()
        commit_date = subprocess.run(["git", "show", "-s", "--format=%ci", self.commit],
                                     capture_output=True, check=True,
                                     encoding="utf-8").stdout.strip()
        commit_msg = subprocess.run(['git', 'show', '-s', '--format=%B', self.commit],
                                    capture_output=True, check=True,
                                    encoding="utf-8").stdout.strip()
        embed = discord.Embed(title="Bot Version",
                              description="Current Bot Version",
                              color=common.random_rgb(self.commit))
        embed.set_footer(text=f"requested by {ctx.author}", icon_url=ctx.author.avatar_url)
        embed.add_field(name="ID", value=self.commit, inline=False)
        embed.add_field(name="Date", value=commit_date, inline=False)
        embed.add_field(name="Changelog", value=commit_msg, inline=False)
        await ctx.send(embed=embed)

    @bot_group.command()
    async def repo(self, ctx):
        """Display the bot repository"""
        await ctx.message.delete()
        embed = discord.Embed(title="Code Repository",
                              description="You can find my source code on [GitDab]("
                                          f"{common.load_config('config.json')[0]['repo_link']}).",
                              color=common.random_rgb())
        embed.set_footer(text=f"requested by {ctx.author}", icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)

    @bot_group.command()
    @commands.is_owner()
    async def update(self, ctx):
        """Update the bot"""
        ctx: commands.Context
        await ctx.message.delete()
        response = subprocess.run(["git", "pull"], capture_output=True, check=True,
                                  encoding="utf-8").stdout.strip()
        embed=discord.Embed(title="Update Report", description=response, color=common.random_rgb())
        embed.set_footer(text=f"requested by {ctx.author}", icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)


def setup(bot):
    """Initialize the cog"""
    bot.add_cog(System(bot))
