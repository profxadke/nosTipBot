#!/usr/bin/env/python
"""Main process for Bot program"""
import subprocess
import logging
import sys
import time
import psutil

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger_handler = logging.StreamHandler()
logger_handler.setFormatter(logging.Formatter('[%(levelname)s] (%(name)s) - %(message)s'))
logger.addHandler(logger_handler)

logger.info("Bot Manager Started!")


def start_bot():
    """Start the bot process"""
    bot_process = subprocess.Popen(["python3", "-B", "bot.py"], stdout=sys.stdout)
    return bot_process


bot = start_bot()
try:
    while True:
        if bot.poll() is not None:
            if bot.returncode == 26:
                logger.info("exit code 26 received, restarting bot!")
                bot = start_bot()
            else:
                break
        time.sleep(1)  # keeps code from overworking.
except KeyboardInterrupt:
    print("Killing Bot Process")
    psutil.Process(bot.pid).kill()
    print("Killed successfully")
