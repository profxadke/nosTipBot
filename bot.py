"""Bot startup code"""
import logging
from pathlib import Path

from discord.ext import commands

from applog.utils import read_logging_config, setup_logging
import core.common as common

logger = logging.getLogger(__name__)
discord_logger = logging.getLogger('discord')

log_config_dict = read_logging_config("applog/logging.yaml")
setup_logging(log_config_dict)

common.prompt_config("config.json", "Enter repository URL: ", "repo_link")
common.prompt_config("config.json", "Enter bot token: ", "token")
common.prompt_config("config.json", "Enter bot prefix: ", "prefix")

config, _ = common.load_config("config.json")

bot = commands.Bot(command_prefix=config['prefix'])


def get_extensions():
    """Gets extension list dynamically"""
    extensions = []
    for file in Path("cogs").glob("**/*.py"):
        extensions.append(str(file).replace("/", ".").replace(".py", ""))
    return extensions


for ext in get_extensions():
    bot.load_extension(ext)

logger.info("starting bot.")
bot.run(config['token'])
